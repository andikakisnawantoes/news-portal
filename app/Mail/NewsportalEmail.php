<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewsportalEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject, $body;

    public function __construct($subject, $body)
    {
        //return this property
        $this->subject = $subject;
        $this->body = $body;
    }

    public function build()
    {
        return $this->view('email.index')->with([
            'subject' => $this->subject
        ]);
    }
}

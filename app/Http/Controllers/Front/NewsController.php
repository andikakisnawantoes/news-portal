<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        $admin = Admin::get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $post_data = Post::with('nCategory')->orderBy('id','desc')->paginate(10);
        return view('front.news.index', compact('post_data','admin_email','admin_phone','logo_school'));
    }
}

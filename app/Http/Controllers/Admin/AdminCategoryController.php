<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class AdminCategoryController extends Controller
{
    public function create()
    {
        return view('admin.category.create');
    }

    public function show()
    {
        //get all data from table category and order it asc
        $categories = Category::get();
        return view('admin.category.index', compact('categories'));
    }

    public function store(Request $request)
    {
        //Validasi request yang masuk dari form tambah kategori
        $request->validate([
            'category_name' => 'required',
            'category_order' => 'required'
        ]);

        //Instansi objek dari class Category
        $category = new Category();

        //Terima inputan $request
        $category->category_name = $request->category_name;

        //Simpan data
        $category->save();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        return view('admin.category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        //Validasi request yang masuk dari form tambah kategori
        $request->validate([
            'category_name' => 'required'
        ]);

        //Pilih data berdasarkan id
        $category = Category::where('id', $id)->first();

        //Terima semua inputan dari $request
        $category->category_name = $request->category_name;
        //Update data
        $category->update();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Diperbarui');
    }

    public function delete($id)
    {
        //Pilih data berdasarkan id
        $category = Category::where('id', $id)->first();

        //Delete data
        $category->delete();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Dihapus');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Mail\NewsportalEmail;
use Hash;
use Auth;

class AdminLoginController extends Controller
{
    //ARAHKAN KE HALAMAN LOGIN
    public function login()
    {
        return view('admin.auth.login');
    }

    //ARAHKAN KE HALAMAN FORGET PASSWORD
    public function forget()
    {
        return view('admin.auth.forget-pass');
    }

    //SUBMIT LOGIN
    public function loginSubmit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::guard('admin')->attempt($credential)){
            return redirect()->route('admin.home');
        }else{
            return redirect()->route('admin.login')->with('error', 'Email or Password is Wrong');
        }
    }

    //LOGOUT
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    //RESET PASSWORD
    public function forgetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $result = Admin::where('email', $request->email)->first();

        if(!$result){
            return redirect()->back()->with('error', 'Email Address Not Found');
        }
    }
    
}

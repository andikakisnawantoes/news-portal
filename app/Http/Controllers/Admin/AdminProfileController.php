<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Hash;
use Auth;

class AdminProfileController extends Controller
{
    
    public function index()
    {
        //redirect to edit profile page
        return view('admin.profile');
    }

    public function profileSubmit(Request $request){

        $admin_data = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        $request->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if($request->password != ''){
            $request->validate([
                'password' => 'required',
                'retype_password' => 'required|same:password'
            ]);
            $admin_data->password = Hash::make($request->password);
        }

        if($request->hasFile('photo')){
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ]);
            
            //remove the file image from public folder
            unlink(public_path('uploads/'.$admin_data->photo));

            //get the extension of image from request
            $ext = $request->file('photo')->extension();

            //name the uploaded image
            $img_name = 'admin'.'.'.$ext;

            //moved image to public folder with its name
            $request->file('photo')->move(public_path('uploads/'), $img_name);

            //update the image in database
            $admin_data->photo = $img_name;
        }

        $admin_data->name = $request->name;
        $admin_data->email = $request->email;
        $admin_data->no_phone = $request->no_phone;
        $admin_data->update();

        return redirect()->back()->with('success', 'Profile Berhasil Diperbarui');
    }
}

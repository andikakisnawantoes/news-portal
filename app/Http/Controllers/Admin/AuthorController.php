<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Hash;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    //masuk ke dashboard sebagai penulis
    public function index()
    {
        return view('admin.author.home');
    }
    
    public function login()
    {
        return view('admin.author.login');
    }
    
    //SUBMIT LOGIN
    public function loginSubmit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ],
        [
            'email.required' => 'Silahkan Isi Alamat Email Anda',
            'password.required' => 'Silahkan Isi Kata Sandi Anda'
        ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::guard('author')->attempt($credential)){
            return redirect()->route('author.home');
        }else{
            return redirect()->route('author.login')->with('error', 'Alamat Email atau Kata Sandi Salah');
        }
    }

    //LOGOUT
    public function logout()
    {
        Auth::guard('author')->logout();
        return redirect()->route('author.login');
    }

    //show all the author's posts
    public function show_author()
    {
        $authors = Author::get();
        return view('admin.author.show_author',compact('authors'));
    }

    public function create()
    {
        return view('admin.author.create');
    }

    public function store(Request $request)
    {
        //instansi objek dari Model Class Author
        $author = new Author();

        //validasi request yang masuk
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:authors',
            'password' => 'required',
            'retype_password' => 'required|same:password',
            'photo' => 'required'
        ]);

        //jika request photo true
        if($request->hasFile('photo')){
            //validasi request
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ]);

            //baca tanggal sekarang
            $now = time();

            //baca extensi asli dari file request
            $ext = $request->file('photo')->extension();

            //simpan foto dengan format author+tanggal+extension
            $final_name = 'author'.$now.'.'.$ext;

            //pindahkan foto ke dalam folder public/uploads
            $request->file('photo')->move(public_path('uploads/'), $final_name);

            //nilai dari photo sama dengan $final_name
            $author->photo = $final_name;

        }

        $author->name = $request->name;
        $author->email = $request->email;
        $author->password = Hash::make($request->password);
        $author->token = '';
        $author->save();

        return redirect()->route('author.show')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id)
    {
        $author = Author::where('id',$id)->first();
        return view('admin.author.edit', compact('author'));
    }

    public function update(Request $request,$id)
    {
        $author = Author::where('id', $id)->first();

        $request->validate([
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('authors')->ignore($author->id)
            ]
        ]);

        if($request->password!=''){
            $request->validate([
                'password' => 'required',
                'retype_password' => 'required|same:password'
            ]);
            $author->password = Hash::make($request->password);
        }

        if($request->hasFile('photo')){
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ]);

            unlink(public_path('uploads/'.$author->photo));

            $now = time();
            $ext = $request->file('photo')->extension();
            $final_name = 'author'.$now.'.'.$ext;
            $request->file('photo')->move(public_path('uploads/'),$final_name);
            $author->photo = $final_name;
        }

        $author->name = $request->name;
        $author->email = $request->email;
        $author->update();

        return redirect()->route('author.show')->with('success', 'Data Berhasil Diperbarui');

    }

    public function delete($id)
    {
        //Pilih data berdasarkan id
        $author = Author::where('id', $id)->first();

        //hapus gambar dari folder public/uploads
        unlink(public_path('uploads/'.$author->photo));

        //Delete data
        $author->delete();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('author.show')->with('success', 'Data Berhasil Dihapus');
    }
}

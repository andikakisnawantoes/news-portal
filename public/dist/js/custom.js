(function($){

    "use strict";

    $(".inputtags").tagsinput('items');

    $(document).ready(function() {
        $('#example1').DataTable();
    });

    $('.icp_demo').iconpicker();

    // $(document).ready(function() {
    //     $('.snote').summernote({
    //         height: 150
    //     });
    // });

    $(document).ready(function() {
        $('#summernote').summernote({
            height: 150
        });
        $('#summernote1').summernote({
            height: 150
        });
    });

    // $('.datepicker').datepicker({ format: "yyyy/mm/dd" });
    // $('.timepicker').timepicker({
    //     icons:
    //     {
    //         up: 'fa fa-angle-up',
    //         down: 'fa fa-angle-down'
    //     }
    // });

})(jQuery);

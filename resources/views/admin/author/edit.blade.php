@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <form action="{{ route('author.update',$author->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Nama Penulis</label>
                                    <input type="text" class="form-control" name="name" value="{{ $author->name }}">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Email Penulis</label>
                                    <input type="email" class="form-control" name="email" value="{{ $author->email }}">
                                </div>
                                <div class="form-group mb-3">
                                    <label class="form-label">Kata Sandi</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group mb-3">
                                    <label class="form-label">Tulis Ulang Sandi</label>
                                    <input type="password" class="form-control" name="retype_password">
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                    <a href="#" class="btn btn-info">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Ganti Gambar</label>
                                    <input type="file" class="form-control" name="photo">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Gambar Penulis</label>
                                    <img src="{{ asset('uploads/'.$author->photo)}}" style="width: 300px;" >
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>  
        </div>
    </div>
@endsection
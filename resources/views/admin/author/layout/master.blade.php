@include('admin.author.layout.head')

<body>
<div id="app">
    <div class="main-wrapper">

        {{-- NAVBAR --}}
        @include('admin.author.layout.navbar')


        {{-- SIDEBAR --}}
        @include('admin.author.layout.sidebar')

        @yield('content')

    </div>
</div>

@include('admin.author.layout.script')

{{-- script for spesific page only --}}
@yield('script')

{{-- notif if any error from controller thrown --}}
@if($errors->any())
    @foreach($errors->all() as $error)
        <script>
            iziToast.error({
                title: '',
                position: 'topRight',
                message: '{{ $error }}',
            });
        </script>
    @endforeach
@endif

{{-- notif if any success in this session thrown from controller --}}
@if(session()->get('success'))
    <script>
        iziToast.success({
            title: '',
            position: 'topRight',
            message: '{{ session()->get('success') }}',
        });
    </script>
@endif
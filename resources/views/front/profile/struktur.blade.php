@extends('front.layout.master')

@section('content')
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Bagan & Struktur Organisasi Sekolah</h2>
                    <nav class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Bagan & Struktur Organisasi Sekolah</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="poll-item">
                        <div class="question">
                            Bagan Organisasi Sekolah
                        </div>
                        <div class="featured-photo">
                            <img src="{{ asset('uploads/'.$img) }}" alt="Post Photo">
                        </div>
                    </div>

                    <div class="poll-item">
                        <div class="question">
                            Struktur Organisasi Sekolah
                        </div>
                        <div class="poll-date">
                            {!! $desk !!}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

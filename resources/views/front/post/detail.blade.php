@extends('front.layout.master')

@section('content')
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $post_detail->post_title }}</h2>
                    <nav class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('news') }}">Berita</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('category.post', $post_detail->category_id) }}">{{ $post_detail->nCategory->category_name}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ $post_detail->post_title }}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
   
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6">
                    <div class="featured-photo">
                        <img src="{{ asset('uploads/'.$post_detail->post_photo) }}" style="widht: 400px;" alt="Post Photo">
                    </div>
                    <div class="sub">
                        <div class="item">
                            <b><i class="fas fa-user"></i></b>
                            <a href="javascript:void;">{{ $user_data->name }}</a>
                        </div>
                        <div class="item">
                            <b><i class="fas fa-edit"></i></b>
                            <a href="javascript:void;">{{ $post_detail->nCategory->category_name }}</a>
                        </div>
                        <div class="item">
                            <b><i class="fas fa-clock"></i></b>
                            @php
                                $update = strtotime($post_detail->updated_at);
                                $post_date = date('d F, Y',$update) ;
                            @endphp
                            {{ $post_date }}
                        </div>
                        <div class="item">
                            <b><i class="fas fa-eye"></i></b>
                            {{ $post_detail->visitors }}
                        </div>
                    </div>
                    <div class="main-text">
                        {!! $post_detail->post_detail !!}
                    </div>
                    <div id="disqus_thread"></div>
                        <script>
                        
                        var disqus_config = function () {
                        this.page.url = window.location.href ;
                        this.page.identifier = {{ $post_detail->id }};
                        };
                        
                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://web-smp-7-samarinda.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    
                    <div class="related-news">
                        <div class="related-news-heading">
                            <h2>Berita Terkait</h2>
                        </div>
                        <div class="related-post-carousel owl-carousel owl-theme">
                            @php $i = 0; @endphp
                            @foreach($related_post_array as $item)
                                @php $i++; @endphp
                                @if($i > 3)
                                    @break
                                @endif
                                @if($item->id == $post_detail->id)
                                    @continue
                                @endif
                                <div class="item">
                                    <div class="photo">
                                        <img src="{{ asset('uploads/'.$item->post_photo) }}" alt="Gambar Berita">
                                    </div>
                                    <div class="category">
                                        <span class="badge bg-success">{{$item->nCategory->category_name}}</span>
                                    </div>
                                    <h3><a href="{{ route('post-detail',$item->id)}}">{{ $item->post_title }}</a></h3>
                                    <div class="date-user">
                                        <div class="user">
                                            @if($item->author_id == 0)
                                                @php
                                                    $user_data = \App\Models\Admin::where('id',$item->admin_id)->first();
                                                @endphp
                                            @else
                                                @php
                                                    $user_data = \App\Models\Author::where('id',$item->author_id)->first();
                                                @endphp
                                            @endif
                                            <a href="javascript:void">{{ $user_data->name }}</a>
                                        </div>
                                        <div class="date">
                                            @php
                                                $update = strtotime($item->updated_at);
                                                $post_date = date('d F, Y',$update) ;
                                            @endphp
                                            {{ $post_date }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 sidebar-col">
                    <div class="sidebar">
                        <div class="widget">
                            <div class="archive-heading">
                                <h2>Arsip</h2>
                            </div>
                            <div class="archive">
                                @php
                                    $arsip_array = [];
                                    $all_post_data = \App\Models\Post::get();
                                    foreach ($all_post_data as $row) {
                                        $post_date = strtotime($row->created_at);
                                        $month = date('m', $post_date);
                                        $month_full = date('F', $post_date);
                                        $year = date('Y', $post_date);
                                        $arsip_array[] = $month.'-'.$month_full.'-'.$year;
                                    }
                                    $arsip_array = array_values(array_unique($arsip_array));
                                @endphp
                                <form action="{{ route('archive.show') }}" method="POST">
                                    @csrf
                                    <select name="archive_month_year" class="form-select" onchange="this.form.submit()">
                                        <option value="">Pilih Bulan</option>
                                        @for($i=0;$i<count($arsip_array);$i++)
                                            @php
                                                $temp_arr = explode('-',$arsip_array[$i]);
                                            @endphp
                                            <option value="{{ $temp_arr[0].'-'.$temp_arr[2] }}">{{ $temp_arr[1] }}, {{ $temp_arr[2] }}</option>
                                        @endfor
                                    </select>
                                </form>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
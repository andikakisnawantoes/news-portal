<!DOCTYPE html>
<html lang="en">

{{-- head here --}}
@include('front.layout.head')

<body>

    {{-- top & heading-area here --}}
    @include('front.layout.top_heading')

    {{-- menu here --}}
    @include('front.layout.menu')

    {{-- Content Page --}}
    @yield('content')

    {{-- footer & copyright here --}}
    @include('front.layout.footer_copyright')

    {{-- custom & scroll-up button here --}}
    @include('front.layout.custom')

</body>
</html>
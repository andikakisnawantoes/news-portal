{{-- <div class="scroll-top">
    <i class="fas fa-angle-up"></i>
</div> --}}

<script src="{{ asset('dist-front/js/custom.js') }}"></script>
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script>
    var today = new Date();
    // today.toLocaleString('default', { month: 'long' });
    var date = today.getDate()+' '+(today.toLocaleString('default', { month: 'long'}))+', '+today.getFullYear();
    document.getElementById("date").innerHTML = date;
    
    $('.magnific').magnificPopup({
	  	type: 'image',
		gallery:{
			enabled:true
		}
	});

    new WOW().init();

    $('.related-post-carousel').owlCarousel({
        loop: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        smartSpeed: 1500,
        margin: 30,
        mouseDrag: true,
        nav: true,
        dots: true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 2
            }
        }
    });
</script>


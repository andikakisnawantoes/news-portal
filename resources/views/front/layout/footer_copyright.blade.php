<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Alamat Sekolah</h2>
                    <div class="list-item">
                        <div class="left">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="right">
                            Jl. Kadrie Oening, Air Hitam, Kec. Samarinda Ulu, <br> 
                            Kota Samarinda, Kalimantan Timur 75243
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Tautan Penting</h2>
                    <ul class="useful-links">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="terms.html">Terms and Conditions</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Kontak</h2>
                    <div class="list-item">
                        <div class="left">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="right">
                            {{ $admin_email }}
                        </div>
                    </div>
                    <div class="list-item">
                        <div class="left">
                            <i class="fas fa-phone-alt"></i>
                        </div>
                        <div class="right">
                            {{ $admin_phone }}
                        </div>
                    </div>
                    <!-- <ul class="social">
            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
            <li><a href=""><i class="fab fa-instagram"></i></a></li>
        </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="copyright">
    Created By STIA DIGITAL TEKNO, 2022
</div>